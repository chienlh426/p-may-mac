//
//  ForgotPasswordViewController.swift
//  PMay
//
//  Created by Chiến Lê on 25/05/2022.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let actionBack = UITapGestureRecognizer(target: self, action: #selector(actionBack))
        btnBack.isUserInteractionEnabled = true
        btnBack.addGestureRecognizer(actionBack)
    }

    @IBAction func actionBack() {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
