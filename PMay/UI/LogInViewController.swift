//
//  LogInViewController.swift
//  PMay
//
//  Created by Chiến Lê on 24/05/2022.
//

import UIKit

class LogInViewController: UIViewController {

    @IBOutlet weak var actionForgotPassword: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let action = UITapGestureRecognizer(target: self, action: #selector(actionForgot))
        actionForgotPassword.isUserInteractionEnabled = true
        actionForgotPassword.addGestureRecognizer(action)
        
    }
    
    @IBAction func actionForgot() {
        self.navigationController?.pushViewController(ForgotPasswordViewController(), animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
